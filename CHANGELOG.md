# ProTV Extras Changelog
Manually curated document of all notable changes to this project sorted by version number in descending order.

Structure used for this document:
```
## Version Number (Publish Date)
### Added
### Changed
### Deprecated
### Removed
### Fixed
```

## 0.7.3-beta (2024-01-27)
- Add alternate url and title inputs to Cyber Red skin.
- Adjust layer of the default screens to Environment instead of Player.
- Update TV prefabs to default to low latency mode and include a non-low-latency option.

## 0.7.2-beta (2024-01-12)
- Move some animations from core ProTV into Extras as core doesn't need them.
- Add placeholder scripts to the Runtime and Editor folders to avoid the warning about assemblies with no scripts.
- 2022 upgrade for prefabs and sprite atlases.

## 0.7.1-beta (2023-11-17)
- Aesthetic tweaks to Cyber Red skin by BluWizard

## 0.7.0-beta (2023-11-09)
- Add Cyber Red skins, contributed by BluWizard

## 0.6.1-beta (2023-11-09)
- Rebuild the VPManager components on prefabs to fix some bad data stored in the prefab.

## 0.6.0-beta (2023-10-20)
- Fix incorrect icon references.
- Update Monochrome prefabs for the latest protv beta.

## 0.5.3-beta (2023-09-21)
- Remove explicit UdonSharp dependency.
- Update version number to a prerelease to avoid some confusion with users who forget to enable prereleases.
- Update Android settings for sprite atlases.

## 0.5.2-beta (2023-08-28)
- Add CONTRIBUTING document.
- Rename TwinRetro items to remove the name in favor the contributing document.
- Update ProTV dependency minimum to 3.0.0-beta.1
- Add new Cyber theme and Cyber Blue skins, contributed by BluWizard
- Rename the folder Resources/UI to Resources/Themes

## 0.5.1-beta (2023-08-25)
- Update package naming from ProTV.Skins to ProTV.Extras
  - This is to be more inclusive of community contributions that aren't theme related, like custom plugins.

## 0.5.0-beta (2023-08-23)
- Initial commit.
- Add TwinRetro theme contributed by MissStabby.
- Add Neon theme contributed by Shyaong.
- Compatible with ProTV 3.0.0-alpha.30 and later.