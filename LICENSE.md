(based on the ISC License)

Copyright (c) 2020-2023 ArchiTechVR

Permission to use, copy, modify, and/or distribute this asset for any
purpose, except for resale in whole or in part, with or without fee is hereby granted, 
provided that the above copyright notice and this permission notice appear in all copies.
For clarity, permission is expressly granted to distribute and sell assets and prefabs
which make use of the functions and/or content of this asset.

THE ASSET IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS ASSET INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS ASSET.